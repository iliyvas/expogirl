package main

import (
	"expoGirl/CarPriceAPI"
	"time"
)

type SecretPhraseStatus string

const (
	Bad  SecretPhraseStatus = "bad"
	Ok   SecretPhraseStatus = "ok"
	Used SecretPhraseStatus = "used"
)

type DialogueResources struct {
	Session     string
	User        *User
	Storage     *Repo
	CarPriceAPI *CarPriceAPI.CarPriceAPI
}

func NewDialogueResources(session string, userId string, repo *Repo) *DialogueResources {
	user, exist := repo.GetUser(userId)
	if !exist {
		user = repo.AddUser(userId)
	}

	return &DialogueResources{session, user, repo, &CarPriceAPI.CarPriceAPI{}}
}

func (r *DialogueResources) SaveHistory(userPhrase string, botAnswer string) *History {
	time := time.Now()
	return r.Storage.AddHistory(r.Session, r.User.UserId, userPhrase, botAnswer, time)
}

func (r *DialogueResources) TryActivateUser(phrase string, history *History) SecretPhraseStatus {
	secretPhrase, exist := r.Storage.GetSecretPhrase(phrase)
	status := Bad
	if exist {
		if secretPhrase.Used {
			status = Used
		} else {
			status = Ok
			r.activateUser(r.User, secretPhrase, history)
		}
	}
	return status
}

func (r *DialogueResources) activateUser(user *User, secretPhrase *SecretPhrase, history *History) {
	r.Storage.AddSecretPhraseUsage(secretPhrase, history)
	r.Storage.ActivateUser(user)
}
