package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

type SessionId string

type Repo struct {
	Dialogues map[SessionId]*Dialogue
}

func (repo *Repo) Init() {
	db := repo.getDb()
	defer db.Close()

	repo.db_exec(db, "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, user_id TEXT, active INTEGER)")
	repo.db_exec(db, "CREATE TABLE IF NOT EXISTS secret_phrases (id INTEGER PRIMARY KEY, phrase TEXT)")
	repo.db_exec(db, "CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, user_phrase TEXT, bot_answer TEXT, "+
		"user_id INTEGER, session TEXT, datetime TEXT)")
	repo.db_exec(db, "CREATE TABLE IF NOT EXISTS secret_phrases_usage (id INTEGER PRIMARY KEY, "+
		"secret_phrase_id INTEGER, history_id INTEGER)")

	repo.Dialogues = make(map[SessionId]*Dialogue)
}

// We need create connection for each operation because of last_insert_rowid() usage.
// Sqlite has no another way to get inserted row id.
// Check docs at https://www.sqlite.org/lang_corefunc.html#last_insert_rowid
func (repo *Repo) getDb() *sql.DB {
	db, err := sql.Open("sqlite3", "./expoGirl.db")
	checkErr(err)
	return db
}

func (repo *Repo) db_exec(db *sql.DB, query string) {
	stmt, err := db.Prepare(query)
	checkErr(err)
	defer stmt.Close()

	_, err = stmt.Exec()
	checkErr(err)
}

func (repo *Repo) GetDialogue(session string) (*Dialogue, bool) {
	dialogue, exist := repo.Dialogues[SessionId(session)]
	return dialogue, exist
}

func (repo *Repo) AddDialogue(session string, userId string) *Dialogue {
	dialogue := NewDialogue(session, userId, repo)
	repo.Dialogues[SessionId(session)] = dialogue
	return dialogue
}

func (repo *Repo) AddUser(userId string) *User {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO users (user_id, active) VALUES (?, 0)")
	checkErr(err)
	defer stmt.Close()

	_, err = stmt.Exec(userId)
	checkErr(err)

	var rowId int
	row := db.QueryRow("SELECT last_insert_rowid()")
	err = row.Scan(&rowId)
	checkErr(err)

	return &User{rowId, userId, false}
}

// TODO: Should I use User instead?
func (repo *Repo) GetUser(userId string) (*User, bool) {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("SELECT id, user_id, active FROM users WHERE user_id = ?")
	checkErr(err)
	defer stmt.Close()

	var rowId int
	var rowUserId string
	var rowActive int
	err = stmt.QueryRow(userId).Scan(&rowId, &rowUserId, &rowActive)

	var user *User
	var exist bool

	switch err {
	case nil:
		active := false
		if rowActive == 1 {
			active = true
		}
		user = &User{rowId, rowUserId, active}
		exist = true

	case sql.ErrNoRows:
		user = nil
		exist = false

	default:
		checkErr(err)
	}

	return user, exist
}

func (repo *Repo) ActivateUser(user *User) {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("UPDATE users SET active = 1 WHERE id = ?")
	checkErr(err)
	defer stmt.Close()

	_, err = stmt.Exec(user.Id)
	checkErr(err)

	user.Active = true
}

func (repo *Repo) GetSecretPhrase(phrase string) (*SecretPhrase, bool) {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("SELECT secret_phrases.id, phrase, secret_phrases_usage.id " +
		"FROM secret_phrases " +
		"LEFT JOIN secret_phrases_usage ON secret_phrases.id = secret_phrase_id " +
		"WHERE phrase = ?")
	checkErr(err)
	defer stmt.Close()

	var rowId int
	var rowPhrase string
	var rowSecPhraseId sql.NullInt64
	err = stmt.QueryRow(phrase).Scan(&rowId, &rowPhrase, &rowSecPhraseId)

	var secretPhrase *SecretPhrase
	var exist bool

	switch err {
	case nil:
		used := rowSecPhraseId.Valid
		secretPhrase = &SecretPhrase{rowId, rowPhrase, used}
		exist = true

	case sql.ErrNoRows:
		secretPhrase = nil
		exist = false

	default:
		checkErr(err)
	}

	return secretPhrase, exist
}

func (repo *Repo) AddSecretPhraseUsage(secretPhrase *SecretPhrase, history *History) {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO secret_phrases_usage (secret_phrase_id, history_id) VALUES (?, ?)")
	checkErr(err)
	defer stmt.Close()

	_, err = stmt.Exec(secretPhrase.Id, history.Id)
	checkErr(err)
}

func (repo *Repo) AddHistory(
	session string, userId string, userPhrase string, botAnswer string, curTime time.Time) *History {
	db := repo.getDb()
	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO history (user_phrase, bot_answer, user_id, session, datetime) " +
		"VALUES (?, ?, ?, ?, ?)")
	checkErr(err)
	defer stmt.Close()

	_, err = stmt.Exec(userPhrase, botAnswer, userId, session, curTime.Format(time.RFC3339))
	checkErr(err)

	var rowId int
	row := db.QueryRow("SELECT last_insert_rowid()")
	err = row.Scan(&rowId)
	checkErr(err)

	return &History{rowId, session, userId, userPhrase, botAnswer, curTime}
}
