package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type App struct {
	Repo   *Repo
	Router *mux.Router
}

func (a *App) Initialize() {
	a.Repo = &Repo{}
	a.Repo.Init()

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/alice", a.aliceHandler).Methods("POST")
}

func (a *App) aliceHandler(w http.ResponseWriter, r *http.Request) {
	AliceHandler(w, r, a.Repo)
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}
