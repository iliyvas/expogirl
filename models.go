package main

import "time"

type User struct {
	Id     int
	UserId string
	Active bool
}

func (u *User) isActive() bool {
	return u.Active
}

type SecretPhrase struct {
	Id     int
	Phrase string
	Used   bool
}

type History struct {
	Id         int
	Session    string
	UserId     string
	UserPhrase string
	BotAnswer  string
	Time       time.Time
}
