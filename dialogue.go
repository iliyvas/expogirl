package main

import (
	"fmt"
	"github.com/looplab/fsm"
)

// Events names
const (
	RequireAuthEvent = "require_auth"
	AuthEvent        = "auth"
	AskEvent         = "ask"
	AnswerEvent      = "answer"
)

// States names
const (
	GreetingState     = "greeting"
	NeedAuthState     = "auth"
	WaitQuestionState = "wait_question"
	WaitAnswerState   = "wait_answer"
)

type Dialogue struct {
	FSM       *fsm.FSM
	Resources *DialogueResources
}

// Natural language units
type NLU struct {
	Tokens []string
}

func NewDialogue(session string, userId string, repo *Repo) *Dialogue {
	d := &Dialogue{FSM: fsm.NewFSM(
		GreetingState,
		fsm.Events{
			{Name: RequireAuthEvent, Src: []string{GreetingState}, Dst: NeedAuthState},
			{Name: AuthEvent, Src: []string{NeedAuthState, GreetingState}, Dst: WaitQuestionState},
			{Name: AskEvent, Src: []string{WaitQuestionState}, Dst: WaitAnswerState},
			{Name: AnswerEvent, Src: []string{WaitAnswerState}, Dst: WaitQuestionState},
		},
		fsm.Callbacks{},
	), Resources: NewDialogueResources(session, userId, repo)}
	return d
}

func (d *Dialogue) greeting() string {
	var greeting string
	if d.Resources.User.isActive() {
		greeting = "Здравствуйте. Чего вы хотели бы узнать?"
		d.FSM.Event(AuthEvent)
	} else {
		greeting = "Здравствуйте. Чтобы начать скажите кодовую фразу."
		d.FSM.Event(RequireAuthEvent)
	}
	return greeting
}

func (d *Dialogue) GetAnswer(question string, nlu *NLU) string {
	history := d.Resources.SaveHistory(question, "")

	var answer string

	switch d.FSM.Current() {
	case GreetingState:
		answer = d.greeting()

	case NeedAuthState:
		secretPhraseStatus := d.Resources.TryActivateUser(question, history)
		switch secretPhraseStatus {
		case Ok:
			answer = "Верная фраза. Чего вы хотели бы узнать?"
			d.FSM.Event(AuthEvent)

		case Bad:
			answer = "Неверная фраза. Попробуйте еще раз."

		case Used:
			answer = "Эта фраза уже использовалась. Попробуйте еще раз."
		}

	case WaitQuestionState:
		answer = d.handleQuestion(question, nlu)
	}

	d.Resources.SaveHistory("", answer)
	return answer
}

func (d *Dialogue) handleQuestion(question string, nlu *NLU) string {
	var answer string

	switch {
	case stringInSlice("цена", nlu.Tokens):
		d.FSM.Event(AskEvent)
		price := d.Resources.CarPriceAPI.GetAvgPrice()
		answer = fmt.Sprintf("Средняя цена автомобиля %d. Что вы еще хотите узнать?", price)
		d.FSM.Event(AnswerEvent)

	case stringInSlice("умеешь", nlu.Tokens):
		d.FSM.Event(AskEvent)
		answer = `Я могу узнать среднюю цену автомобиля. Для этого скажите "цена".`
		d.FSM.Event(AnswerEvent)

	default:
		answer = "Я вас не поняла. Попробуйте еще раз."
	}

	return answer
}
