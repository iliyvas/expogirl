package main

type AliceRequestJson struct {
	Request struct {
		Command string `json: "command""`
		NLU     struct {
			Tokens []string `json: "tokens"`
		}
	} `json: "request"`
	Session struct {
		SessionId string `json:"session_id"`
		MessageId int    `json:"message_id"`
		UserId    string `json:"user_id"`
	} `json: "session"`
}

func (reqJson *AliceRequestJson) GetQuestion() string {
	return reqJson.Request.Command
}

func (reqJson *AliceRequestJson) GetSessionId() string {
	return reqJson.Session.SessionId
}

func (reqJson *AliceRequestJson) GetMessageId() int {
	return reqJson.Session.MessageId
}

func (reqJson *AliceRequestJson) GetUserId() string {
	return reqJson.Session.UserId
}

func (reqJson *AliceRequestJson) ToNLU() *NLU {
	return &NLU{reqJson.Request.NLU.Tokens}
}

type AliceResponse struct {
	Text       string `json:"text"`
	EndSession bool   `json:"end_session"`
}

type AliceSession struct {
	SessionId string `json:"session_id"`
	MessageId int    `json:"message_id"`
	UserId    string `json:"user_id"`
}

type AliceResponseJson struct {
	Response AliceResponse `json:"response"`
	Session  AliceSession  `json:"session"`
	Version  string        `json:"version"`
}

func NewAliceResponseJson(text string, sessionId string, messageId int, userId string) *AliceResponseJson {
	return &AliceResponseJson{
		Response: AliceResponse{text, false},
		Session:  AliceSession{sessionId, messageId, userId},
		Version:  "1.0",
	}
}
