package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

func AliceHandler(w http.ResponseWriter, r *http.Request, repo *Repo) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1<<20))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	var request AliceRequestJson
	if err := json.Unmarshal(body, &request); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	dialogue, exist := repo.GetDialogue(request.GetSessionId())
	if !exist {
		dialogue = repo.AddDialogue(request.GetSessionId(), request.GetUserId())
	}

	answer := dialogue.GetAnswer(request.GetQuestion(), request.ToNLU())

	response := NewAliceResponseJson(answer, request.GetSessionId(), request.GetMessageId(), request.GetUserId())
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err)
	}
}
